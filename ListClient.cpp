#include <iostream>
#include "List.h"

using namespace std;

int main()
{

 List L1, L2; //Declare two list objects, L1 and L2


 cout << "Welcome to my List ADT client" <<endl <<endl;

 //Do some stuff with L1, L2, ... (Eg. cout<<L1.size(); )
 // ...
 

 cout << L1.size();
 L1.insert(1,1);
 L1.insert(2,2);
 L1.insert(3,3);
 L1.insert(4,4);
 L1.insert(5,5);
 cout << L1.get(2) << endl;

 L1.clear();
cout << L1.size() << endl; 


}
